output aws_account {
  value = data.aws_caller_identity.current
}
output "base_url" {
  value = aws_api_gateway_deployment.api.invoke_url
}
/*
output aws_region {
  value = data.aws_region.current
}
output aws_partition {
  value = data.aws_partition.current
}
output aws_regions {
  value = data.aws_regions.current.names
}
output aws_billing_account {
  value = data.aws_billing_service_account.main
}
*/

