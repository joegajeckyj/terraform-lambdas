resource aws_s3_bucket bucket {
  bucket = var.bucket_name
  acl    = "private"

  versioning {
    enabled = true
  }

  tags = {
    Name        = var.bucket_name
    Environment = var.environment
  }
}

resource aws_s3_bucket_object object {
  bucket = var.bucket_name
  key    = var.lambda_src
  source = var.lambda_src_path

  etag = filemd5(var.lambda_src_path)

  depends_on = [
    aws_s3_bucket.bucket
  ]
  
}