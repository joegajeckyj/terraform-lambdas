
resource aws_lambda_function function {
   function_name = var.lambda_name

   # The bucket name as created earlier with "aws s3api create-bucket"
   s3_bucket = aws_s3_bucket.bucket.id
   s3_key    = aws_s3_bucket_object.object.id
   s3_object_version = aws_s3_bucket_object.object.version_id

   # "main" is the filename within the zip file (main.js) and "handler"
   # is the name of the property under which the handler function was
   # exported in that file.
   handler = "lambda_function.lambda_handler"
   runtime = "python3.8"

   role = aws_iam_role.lambda_exec.arn
}
