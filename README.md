# Terraform Deploy Lambda #

This repo is intended to create a method of deploying and updating lambda functions via terraform

### What is this repository for? ###

* Testing terrform resources for creating lambda
* Created from guide article <https://learn.hashicorp.com/tutorials/terraform/lambda-api-gateway>
* Version 1.0

### How do I get set up? ###

* Setup 
    * I have been using the amazon linux instance in WSL2 in windows for testing, this is documented [here](https://aws.amazon.com/blogs/developer/developing-on-amazon-linux-2-using-windows/)
    * Terraform [here](https://learn.hashicorp.com/tutorials/terraform/install-cli)
* Configuration
    * Currently is a stand alone setup **Please check you AWS config before running**
* Dependencies
    * Terraform install on to the instance see above
* How to run tests
    * terraform init
    * terraform validate
* Deployment instructions
    * terraform apply
    * for updating the lambda functions you can either upload a new zip to the s3 which is created for you, or you can amend the downloaded zip in the repo code in **/src/**

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
    * Joe Gajeckyj
* Other community or team contact
    * Simon Hanmer