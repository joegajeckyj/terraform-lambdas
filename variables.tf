variable environment {
    type = string
    default = "dev"    
}
variable bucket_name {
    type = string
}
variable lambda_name {
  type        = string
  default     = "hello-world"
}
variable lambda_src {
  type        = string
  default     = "hello-world.zip"
}
variable lambda_version {
  type        = string
  default     = "1.0.0"
}
variable lambda_src_path {
  type        = string
  default     = "./src/hello-world.zip"
  description = "description"
}

