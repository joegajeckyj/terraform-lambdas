
 # IAM role which dictates what other AWS services the Lambda function
 # may access.
resource aws_iam_role lambda_exec {
   name = "lambda_execution_role"

   assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
        {
        Action = "sts:AssumeRole"
        Principal = {
            Service = "lambda.amazonaws.com"
        },
        Effect = "Allow"
        Sid = ""
        }
        ]
    })

}

resource "aws_lambda_permission" "apigw" {
   statement_id  = "AllowAPIGatewayInvoke"
   action        = "lambda:InvokeFunction"
   function_name = aws_lambda_function.function.function_name
   principal     = "apigateway.amazonaws.com"

   # The "/*/*" portion grants access from any method on any resource
   # within the API Gateway REST API.
   source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}